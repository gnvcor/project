'use strict';

import DocReady from 'es6-docready';
import Raphael from '../../../vendor/raphael.js/raphael.js';

export class InteractiveMap {
    constructor() {
        this.width = 1000;
        this.height = 500;
        this.attrMap = {
            0 : {
                stroke: "#ffffff",
                "stroke-width": 3,
                fill: "#dcdcde"
            },
            1 : {
                stroke: "#ffffff",
                "stroke-width": 3,
                fill: "#dcdcde"
            },
            2 : {
                stroke: "#ffffff",
                "stroke-width": 3,
                fill: "#dcdcde"
            },
            3 : {
                stroke: "#ffffff",
                "stroke-width": 3,
                fill: "#dcdcde"
            },
            4 : {
                stroke: "#ffffff",
                "stroke-width": 3,
                fill: "#dcdcde"
            },
            5 : {
                stroke: "#ffffff",
                "stroke-width": 3,
                fill: "#dcdcde"
            },
            6 : {
                stroke: "#ffffff",
                "stroke-width": 3,
                fill: "#dcdcde"
            },
            7 : {
                stroke: "#ffffff",
                "stroke-width": 3,
                fill: "#dcdcde"
            },
            8 : {
                stroke: "#ffffff",
                "stroke-width": 3,
                fill: "#dcdcde"
            }
        };
        this.objectParams = {
            0 : {
                name: 'Центральный',
                style: {
                    'font-size': '16 !important',
                    fill: '#fff',
                    stroke: 'none',
                    'font-family': 'Roboto !important',
                    'font-weight': '400 !important'
                },
                x: 155,
                y: 270
            },
            1 : {
                name: 'Северо-западный',
                style: {
                    'font-size': '16 !important',
                    fill: '#fff',
                    stroke: 'none',
                    'font-family': 'Roboto !important',
                    'font-weight': '400 !important'
                },
                x: 288,
                y: 225
            },
            2 : {
                name: 'Уральский',
                style: {
                    'font-size': '16 !important',
                    fill: '#fff',
                    stroke: 'none',
                    'font-family': 'Roboto !important',
                    'font-weight': '400 !important'
                },
                x: 405,
                y: 330
            },
            3 : {
                name: 'Приволжский',
                style: {
                    'font-size': '16 !important',
                    fill: '#fff',
                    stroke: 'none',
                    'font-family': 'Roboto !important',
                    'font-weight': '400 !important'
                },
                x: 230,
                y: 342
            },
            4 : {
                name: 'Северо-Кавказский',
                style: {
                    'font-size': '16 !important',
                    fill: '#333333',
                    stroke: 'none',
                    'font-family': 'Roboto !important',
                    'font-weight': '400 !important'
                },
                x: -23,
                y: 450

            },
            5 : {
                name: 'Южный',
                style: {
                    'font-size': '16 !important',
                    fill: '#fff',
                    stroke: 'none',
                    'font-family': 'Roboto !important',
                    'font-weight': '400 !important'
                },
                x: 115,
                y: 370
            },
            6 : {
                name: 'Крымский',
                style: {
                    'font-size': '16 !important',
                    fill: '#333333',
                    stroke: 'none',
                    'font-family': 'Roboto !important',
                    'font-weight': '400 !important'
                },
                x: -50,
                y: 318
            },
            7 : {
                name: 'Дальневосточный',
                style: {
                    'font-size': '16 !important',
                    fill: '#fff',
                    stroke: 'none',
                    'font-family': 'Roboto !important',
                    'font-weight': '400 !important'
                },
                x: 800,
                y: 290
            },
            8 : {
                name: 'Сибирский',
                style: {
                    'font-size': '16 !important',
                    fill: '#fff',
                    stroke: 'none',
                    'font-family': 'Roboto !important',
                    'font-weight': '400 !important'
                },
                x: 592,
                y: 378
            }
        };
    }

    labelPath( pathObj, text, textattr, x, y ){
        var textObj = pathObj.paper.text( x, y, text ).attr( textattr );
        return textObj;
    }

    mapInit() {
        DocReady( () => {

            var mapSelector = document.getElementById('mapPaper');

            if (mapSelector) {
                var R = Raphael("mapPaper");
                var self = this;
                R.setViewBox(0, 0, this.width, this.height, true);
                R.setSize('100%', '100%');
                var i = 0,
                    area = {},
                    areaText = {};

                var oAreas = $(".mapArea");
                var shadow;
                var text;
                var shadowClick;
                var count;
                var mouseover = 0;

                $.each(oAreas, function(){
                    area[i] = R.path($(this).attr("data-path")).attr(self.attrMap[i]);
                    areaText[i] = self.labelPath( area[i], self.objectParams[i].name, self.objectParams[i].style, self.objectParams[i].x, self.objectParams[i].y);
                    i++;
                });

                $("[data-select-sort]").on("change", function() {
                    shadowClick.remove();
                    count = $(this).val();
                    area[$(this).val()].toFront();
                    shadowClick = area[$(this).val()].glow({
                        color: '#000000',
                        width: 20,
                        opacity: 0.1
                    });
                    areaText[$(this).val()].toFront();
                    $(".help__city-item-map").removeClass("active");
                    $(".help__cont-city").find("[data-id-"+$(this).val()+"]").addClass("active");
                });

                for (var j=0; j<i; j++) {
                    (function (o, j) {
                        o[0].style.cursor = "pointer";

                        if (j == 0) { //Центральный федеральный округ по дефолту
                            count = j;
                            area[j].toFront();
                            area[j].attr('fill', '#f16223');
                            shadowClick = area[j].glow({
                                color: '#000000',
                                width: 20,
                                opacity: 0.1
                            });
                            areaText[j].toFront();
                        }

                        console.log(o[0]);

                        area[j].hover(function() {
                            if (mouseover == 0){
                                mouseover = 1;
                                console.log(mouseover);
                                if (count == j) {
                                    return false;
                                } else {
                                    area[j].toFront();
                                    shadow = area[j].glow({
                                        color: '#000000',
                                        width: 20,
                                        opacity: 0.1
                                    });
                                    areaText[j].toFront();
                                    area[j].attr('fill', '#f16223');
                                }
                            }
                        }, function() {
                            if (shadow != undefined) {
                                if (shadow.length > 0) {
                                    area[j].attr('fill', '#f16223');
                                    shadow.remove();
                                }
                            }
                            if (count != undefined) {
                                shadowClick.remove();
                                area[count].toFront();
                                shadowClick = area[count].glow({
                                    color: '#000000',
                                    width: 20,
                                    opacity: 0.1
                                });
                                areaText[count].toFront();
                                if (count == j) {
                                    area[j].attr('fill', '#f16223');
                                } else {
                                    area[j].attr('fill', '#dcdcde');
                                }
                            }

                            mouseover = 0;
                        });
                        o[0].onclick = function () {
                            console.log(area);
                            if (shadowClick == undefined) {
                                count = j;
                                shadow.remove();
                                area[j].toFront();
                                shadowClick = area[j].glow({
                                    color: '#000000',
                                    width: 20,
                                    opacity: 0.1
                                });
                                areaText[j].toFront();

                                for (var k=0; k<9; k++) {
                                    if (count == j) {
                                        area[k].attr('fill', '#f16223');
                                        area[count].attr('fill', '#f16223');
                                    } else {
                                        area[k].attr('fill', '#dcdcde');
                                    }
                                }

                                $(".help__city-item-map").removeClass("active");
                                $(".help__cont-city").find("[data-id-"+j+"]").addClass("active");
                                $("[data-select-sort]").val(j).trigger("change");
                                $('html,body').animate({scrollTop: $("#map-result").offset().top - $(".header").outerHeight() - $(".script-height").outerHeight() - (($(".script-height").length > 0) ? 20 : 0)},'slow');
                            } else {
                                shadowClick.remove();
                                count = j;
                                shadow.remove();
                                area[j].toFront();
                                shadowClick = area[j].glow({
                                    color: '#000000',
                                    width: 20,
                                    opacity: 0.1
                                });
                                areaText[j].toFront();

                                for (var k=0; k<9; k++) {
                                    if (count == j) {
                                        area[k].attr('fill', '#dcdcde');
                                        area[count].attr('fill', '#f16223');
                                    } else {
                                        area[k].attr('fill', '#f16223');
                                    }
                                }

                                $(".help__city-item-map").removeClass("active");
                                $(".help__cont-city").find("[data-id-"+j+"]").addClass("active");
                                $("[data-select-sort]").val(j).trigger("change");
                                $('html,body').animate({scrollTop: $("#map-result").offset().top - $(".header").outerHeight() - $(".script-height").outerHeight() - (($(".script-height").length > 0) ? 20 : 0)},'slow');
                            }
                        };
                    })(area[j], j);
                }
            }
        } );
    }
}

let map = new InteractiveMap();

map.mapInit();