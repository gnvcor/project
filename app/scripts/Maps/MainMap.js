'use strict';

import DocReady from 'es6-docready';

export class MainMap {
    constructor() {

    }

    initMap(selector) {
        var map;

        DocReady(() => {
            var mapSelector = document.getElementById(selector);

            if (mapSelector) {
                map = new google.maps.Map(document.getElementById(selector), {
                    zoom: 12,
                    center: {lat: 52.520, lng: 13.410},
                    scrollwheel: false,
                    disableDoubleClickZoom: false
                });
            }
        });
    }
}

let mainMap = new MainMap();

google.maps.event.addDomListener(window, 'load', mainMap.initMap("map"));

let helpMap = new MainMap();

google.maps.event.addDomListener(window, 'load', helpMap.initMap("map"));