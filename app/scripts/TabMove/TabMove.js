'use strict';

import DocReady from 'es6-docready'

export class TabMove {
    constructor() {

    }

    oneScroll() {
        DocReady(() => {
            var _this = this;

            $(".btn--constructor").on("click", function() {
                let anchor = $(this).attr("data-link");
                _this.scrollToAnchor(anchor);
            });
        });
    }

    scrollToAnchor(anchor_id) {
        var tag = $(anchor_id);
        $('html,body').animate({scrollTop: tag.offset().top - $(".header").outerHeight() - $(".script-height").outerHeight() - (($(".script-height").length > 0) ? 20 : 0)},'slow');
    }

    getAnchor() {
        var _this = this;
        let container = $(".tabs--static-page");
        let item = container.find(".tabs__link");

        item.each(function() {
            var self = $(this);
            var thisAnchor = $(this).attr("data-link");

            $(this).on("click", function() {
                //item.parent().removeClass("active");
                //$(this).parent().addClass("active");
                let anchor = self.attr("data-link");
                _this.scrollToAnchor(anchor);
            });

            $(window).scroll(function () {
                if ($(this).scrollTop() >= $(thisAnchor).offset().top - $(".header").outerHeight() - $(".script-height").outerHeight() - (($(".script-height").length > 0) ? 20 : 0)) {
                    item.parent().removeClass("active");
                    self.parent().addClass("active");
                }
            });
        });
    }

    tabMove() {
        DocReady(() => {
            //this.oneScroll();
            this.getAnchor();

            $(window).scroll(function () {
                if ($(this).scrollTop() > $(".nav-block--help-page").outerHeight() - $(".header-tabs--fixed").outerHeight()) {
                    $(".header-tabs__fix-cont").addClass("active");
                } else {
                    $(".header-tabs__fix-cont").removeClass("active");
                }

                /*if ($(this).scrollTop() >= $("#gross").offset().top) {
                    $(".tabs__item--retail").removeClass("active");
                    $(".tabs__item--gross").addClass("active");
                } else {
                    $(".tabs__item--retail").addClass("active");
                    $(".tabs__item--gross").removeClass("active");
                }*/
            });
        });
    }
}

let move = new TabMove();

move.tabMove();
move.oneScroll();