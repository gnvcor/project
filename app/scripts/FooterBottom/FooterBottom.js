'use strict';

import DocReady from 'es6-docready';

export class FooterBottom {
    constructor() {

    }

    footerMove() {
        DocReady( () => {
            $(".main").css({
                "min-height" : $(window).height() - $(".footer").height() - $(".header").height()
            });
        } );
    }
}

let footer = new FooterBottom();

footer.footerMove();

$(window).on("resize", () => {
    footer.footerMove();
});