'use strict';

import DocReady from 'es6-docready';

export class BackTop {
    constructor() {

    }

    getTop() {
        DocReady( () => {
            var top_show = 400;
            $(window).scroll(function () {
                if ($(this).scrollTop() > top_show) $('#top').fadeIn();
                else $('#top').fadeOut();
            });
            $('#top').click(function () {
                $('body, html').animate({
                    scrollTop: 1
                }, 'slow');
            });
        } );
    }
}

let top = new BackTop();

top.getTop();