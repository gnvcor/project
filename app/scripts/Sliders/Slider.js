'use strict';

import DocReady from 'es6-docready';
import '../../../vendor/owl.carousel/dist/owl.carousel.js';

export class Slider {
    constructor(selector, items, nav, margin, loop, responsiveLogic, center) {
        this.selector = selector;
        this.items = items;
        this.nav = nav;
        this.margin = margin;
        this.loop = loop;
        this.responsiveLogic = responsiveLogic;
        this.responsive = {
            0:{
                items:1
            },
            768:{
                items: this.selector == '.constructor__big-slider-init' || this.selector == '.constructor__small-slider-init' ? 3 : 2
            },
            960:{
                items: this.selector == '.constructor__big-slider-init' || this.selector == '.constructor__small-slider-init' || this.selector == '.comparison-block__slider-init' ? 3 : 4
            }
        };
        this.center = center;
    }

    sliderInit() {
        DocReady( () => {
            $(this.selector).owlCarousel({
                items : this.items,
                nav : this.nav,
                margin : this.margin,
                loop : this.loop,
                responsive : this.responsiveLogic == true ? this.responsive : {},
                center : this.center,
                mouseDrag : false
            });
        } );
    }
}

let mainSlider = new Slider('.main-page-slider__init', 1, true, 0, true, false, false);

mainSlider.sliderInit();

let mainSliderParnters = new Slider('.partner-slider__init', 5, true, 0, true, false, false);

mainSliderParnters.sliderInit();

let catalogSliderGood = new Slider('.slider-good-block__init', 4, true, 24, true, true, false);

catalogSliderGood.sliderInit();

let detailSliderGood = new Slider('.detail__slider-init', 4, true, 24, false, true, false);

detailSliderGood.sliderInit();

let constructorBigSlider = new Slider('.constructor__big-slider-init', 3, true, 45, true, true, true);

constructorBigSlider.sliderInit();

let constructorSmallSlider = new Slider('.constructor__small-slider-init', 3, true, 45, true, true, true);

constructorSmallSlider.sliderInit();

let comparisonSlider = new Slider('.comparison-block__slider-init', 3, true, 0, true, true, false);

comparisonSlider.sliderInit();

let aboutPartnersSlider = new Slider('.about__slider-init', 4, true, 0, true, true, false);

aboutPartnersSlider.sliderInit();