'use strict';

import DocReady from 'es6-docready';

export class SearchHeaderInput {
    constructor() {

    }

    open() {
        DocReady(() => {
            let current = $("[data-search]");

            current.on("click", function() {
                $(".input-search").addClass("active");
                $(".input-search__input").addClass("active");
                $(".input-search__close").addClass("active");
            });
        });
    }

    close() {
        DocReady(() => {
            let current = $("[data-search-close]");

            current.on("click", function() {
                $(".input-search").removeClass("active");
                $(".input-search__input").removeClass("active");
                $(".input-search__close").removeClass("active");
            });
        });
    }
}

let add = new SearchHeaderInput();

add.open();
add.close();