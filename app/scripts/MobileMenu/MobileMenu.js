'use strict';

import DocReady from 'es6-docready';

export class MobileMenu {
    constructor() {

    }

    open() {
        DocReady(() => {
            let current = $("[data-mobile-menu]");

            current.on("click", function() {
                current.toggleClass("active");
                $(".mobile-menu").toggleClass("active");
            });
        });
    }
}

let open = new MobileMenu();

open.open();