'use strict';

import DocReady from 'es6-docready';

export class Filter {
    constructor() {

    }

    open() {
        DocReady(() => {
            let container = $(".filter");
            let item = container.find(".filter__item");

            item.each(function() {
                let title = $(this).find(".filter__title");
                let block = $(this).find(".filter__block");

                title.on("click", function() {
                    title.toggleClass("active");
                    block.toggleClass("active");
                });
            });
        });
    }
}

let open = new Filter();

open.open();