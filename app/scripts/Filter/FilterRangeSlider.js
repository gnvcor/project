'use strict';

import DocReady from 'es6-docready';
import '../../../vendor/ionrangeslider/js/ion.rangeSlider.js';

export class FilterRangeSlider {
    constructor(selector, type, dataIonMin, dataIonMax) {
        this.selector = selector;
        this.type = type;
        this.dataIonMin = dataIonMin;
        this.dataIonMax = dataIonMax;
    }

    init() {
        let self = this;
        let min = this.dataIonMin;
        let max = this.dataIonMax;

        DocReady(() => {
            $(this.selector).ionRangeSlider({
                type: this.type,
                onStart: function (data) {
                    $(min).val(data.from);
                    $(max).val(data.to);
                },
                onChange: function (data) {
                    $(min).val(data.from);
                    $(max).val(data.to);
                },
            });

            self.sliderUpdateMin();
            self.sliderUpdateMax();
        });
    }

    sliderUpdateMin() {
        let selectorIn = this.selector;

        $(this.dataIonMin).on("keyup", function() {
            let _slider = $(selectorIn).data("ionRangeSlider");
            let thisVal = $(this).val();

            _slider.update({
                from: thisVal
            });
        });
    }

    sliderUpdateMax() {
        let selectorIn = this.selector;

        $(this.dataIonMax).on("keyup", function() {
            let _slider = $(selectorIn).data("ionRangeSlider");
            let thisVal = $(this).val();

            _slider.update({
                to: thisVal
            });
        });
    }
}

let rangePrice = new FilterRangeSlider("[data-range-price]", "double", "[data-ion-min-price]", "[data-ion-max-price]");

rangePrice.init();

let rangeLight = new FilterRangeSlider("[data-range-light]", "double", "[data-ion-min-light]", "[data-ion-max-light]");

rangeLight.init();

let rangePower = new FilterRangeSlider("[data-range-power]", "double", "[data-ion-min-power]", "[data-ion-max-power]");

rangePower.init();