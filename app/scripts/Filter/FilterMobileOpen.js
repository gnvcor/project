'use strict';

import DocReady from 'es6-docready';

export class FilterMobileOpen {
    constructor() {

    }

    open() {
        DocReady(() => {
            let selector = $("[data-open-filter]")

            selector.on("click", function() {
                $(".filter").toggleClass("active");
                $("body").toggleClass("body-overflow");
            });
        });
    }
}

let open = new FilterMobileOpen();

open.open();