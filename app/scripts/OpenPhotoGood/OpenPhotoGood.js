'use strict';

import DocReady from 'es6-docready'

export class OpenPhotoGood {
    constructor(selector, textOpen, textClose) {
        this.selector = selector;
        this.textOpen = textOpen;
        this.textClose = textClose;
    }

    open() {
        DocReady(() => {
            var _this = this;
            let current = $(this.selector);

            current.on("click", function() {
                current.toggleClass("active");
                $(".detail__slider-hide").toggleClass("open");

                let t1 = _this.textOpen;
                let t2 = _this.textClose;
                let self = $(this);

                if ($(this).text() == t1){
                    $(self).text(t2);
                } else {
                    $(self).text(t1);
                }
            });
        });
    }
}

let open = new OpenPhotoGood("[data-open-photo]", "Показать все фото", "Скрыть все фото");

open.open();

let addAddress = new OpenPhotoGood("[data-open-address]", "Добавить новый адрес", "Скрыть новый адрес");

addAddress.open();