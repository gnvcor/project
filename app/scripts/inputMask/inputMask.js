'use strict';

import DocReady from 'es6-docready';
import '../../../vendor/jquery.inputmask/dist/jquery.inputmask.bundle.js';

export class inputMask {
    constructor(selector, mask) {
        this.selector = selector;
        this.mask = mask;
    }

    maskInit() {
        DocReady( () => {
            $(this.selector).inputmask({
                mask: this.mask,
                showMaskOnFocus: false
            });
        } );
    }
}

let mask = new inputMask('[data-coupon]', "999 9999 9999 9999");

mask.maskInit();

let maskPhone = new inputMask('[data-tel]', "+7 999 99-99-99");

maskPhone.maskInit();