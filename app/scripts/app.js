import Slider from './Sliders/Slider'; //Slider init

import MainMap from './Maps/MainMap'; //Main page map

import InteractiveMap from './Maps/InteractiveMap'; //Interactive map

import SearchHeaderInput from './SearchHeaderInput/SearchHeaderInput'; //Add class input type = text

import MobileMenu from './MobileMenu/MobileMenu'; //Open mobile menu

import OpenText from './OpenText/OpenText'; //Open hide text

import OpenDescriptionTable from './OpenDescriptionTable/OpenDescriptionTable'; //Open hide table description

import OpenPhotoGood from './OpenPhotoGood/OpenPhotoGood'; //Open hide photo good

import Filter from './Filter/Filter'; //Open filter item

import FilterMobileOpen from './Filter/FilterMobileOpen'; //Open filter mobile

import FilterRangeSlider from './Filter/FilterRangeSlider'; //Filter range slider

import Select from './Select/Select'; //Select init

import Modal from './Modal/Modal'; //Modal init (fancybox)

import Counter from './Detail/Counter'; //Detail counter price

import ZoomPhoto from './Detail/ZoomPhoto'; //Init zoom detail picture

import FooterBottom from './FooterBottom/FooterBottom'; //Move footer bootom

import Calculator from './Calculator/Calculator'; //Calculator

import TabMove from './TabMove/TabMove'; //Tab move

import BackTop from './BackTop/BackTop'; //Back top button

import inputMask from './inputMask/inputMask'; //Input mask init