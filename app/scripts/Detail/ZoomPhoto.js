'use strict';

import DocReady from 'es6-docready';
import '../../../app/library/jquery.elevatezoom.js';
import Counter from './Counter';

export default class ZoomPhoto extends Counter{
    constructor() {
        super();
    }

    zoomInit(selector, gallery) {
        DocReady( () => {
            $(selector).elevateZoom({
                cursor: "crosshair",
                zoomType: "inner",
                gallery: gallery,
                galleryActiveClass: "active"
            });
        } );
    }

    hotFixWidth() {
        $(".zoomContainer, .zoomWindowContainer, .zoomWindowContainer div").css({"width" : $(".detail__picture").width()});
    }
}

let zoom = new ZoomPhoto();

zoom.zoomInit('#zoom_01', 'gallery_01');

$(window).on("resize", () => {
    zoom.hotFixWidth();
});