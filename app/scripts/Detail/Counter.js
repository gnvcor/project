'use strict';

import DocReady from 'es6-docready';
import Convert from '../Convert/Convert'

export default class Counter extends Convert{
    constructor() {
        super();
        this.count = 1;
        this.newPrice = 0;
    }

    setValue() {
        DocReady(() => {
            this.minus();
            this.plus();
        });
    }

    minus() {
        var self = this;
        var defaultPrice = Number($(".detail__default-value").attr("data-detail-price"));
        let selector = $("[data-counter-minus]");
        this.newPrice = defaultPrice;

        selector.on("click", () => {
            if (self.count <= 1) {
                return false;
            } else {
                self.count--;
                self.newPrice = self.newPrice - defaultPrice;

                $("[data-detail-price-result]").html(super.getConvertValue(Number(self.newPrice)));
                $("[data-counter-value]").html(self.count);
            }
        });
    }

    plus() {
        var self = this;
        var defaultPrice = Number($(".detail__default-value").attr("data-detail-price"));
        let selector = $("[data-counter-plus]");
        this.newPrice = defaultPrice;

        selector.on("click", () => {
            self.count++;
            self.newPrice = self.newPrice + defaultPrice;

            $("[data-detail-price-result]").html(super.getConvertValue(Number(self.newPrice)));
            $("[data-counter-value]").html(self.count);
        });
    }
}

//let count = new Counter();

//count.setValue();