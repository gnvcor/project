'use strict';

import DocReady from 'es6-docready';

export default class NumberAnimate {
    constructor() {

    }

    getAnimate(selector, value, fixed) {
        DocReady(() => {
            $(selector).animate({ num: value }, {
                duration: 500,
                step: function (num){
                    this.innerHTML = ((num - 1) + 1).toFixed(fixed)
                }
            });
        });
    }
}