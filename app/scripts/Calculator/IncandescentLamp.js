'use strict';

import DocReady from 'es6-docready';
import SelectTypeLamp from './SelectTypeLamp';
import NumberAnimate from './NumberAnimate';

export default class IncandescentLamp extends SelectTypeLamp {
    constructor() {
        super();

        this.priceArray = {
            20 : 19,
            40 : 29,
            50 : 29,
            60 : 39,
            75 : 39,
            100 : 49,
            150 : 59
        };

        this.animate = new NumberAnimate();

        this.incandescentHoursWorking = 1000;
    }

    getCollection() {
        for (var key in this.priceArray) {
            if ($("[data-price-select]").val() == key) {
                return {
                    power : key,
                    price : this.priceArray[key]
                }
            }
        }
    }

    initValue() {
        if ($("[data-price-incandescent]").length > 0) {
            $("[data-price-incandescent]").text(this.getCollection().price);
        }
        $("[data-equivalent]").text($("[data-price-select]").val());
        //this.animate.getAnimate("[data-price-incandescent]", this.getCollection().price, 0);
        //this.animate.getAnimate("[data-equivalent]", $("[data-price-select]").val(), 0);
        $("#equivalent-price").attr("data-equivalent", $("[data-price-select]").val());

        if ($("[data-equivalent-power]").length > 0) {
            $("[data-equivalent-power]").text(super.getCollectionType().power);
        }
        if ($("[data-equivalent-price]").length > 0) {
            $("[data-equivalent-price]").text(super.getConvertValue(super.getCollectionType().price));
        }
        //this.animate.getAnimate("[data-equivalent-power]", super.getCollectionType().power, 0);
        //this.animate.getAnimate("[data-equivalent-price]", super.getConvertValue(super.getCollectionType().price), 0);

        if ($("[data-default-energy]").length > 0) {
            $("[data-default-energy]").text(super.resultCalc(this.getCollection().power, this.month, this.rate).energy);
        }
        if ($("[data-default-money]").length > 0) {

            $("[data-default-money]").text(super.resultCalc(this.getCollection().power, this.month, this.rate).money);
        }
        //this.animate.getAnimate("[data-default-energy]", super.resultCalc(this.getCollection().power, this.month).energy, 2);
        //this.animate.getAnimate("[data-default-money]", super.resultCalc(this.getCollection().power, this.month).money, 2);

        if ($("#default-energy-graph").length > 0) {
            $("#default-energy-graph").css({width : super.resultCalc(this.getCollection().power, this.month, this.rate).energy * 100 / 162+"%"});
        }
        if ($("#default-money-graph").length > 0) {
            $("#default-money-graph").css({width : super.resultCalc(this.getCollection().power, this.month, this.rate).money * 100 / 738.72+"%"});
        }

        if ($("[data-equivalent-energy]").length > 0) {
            $("[data-equivalent-energy]").text(super.resultCalc(super.getCollectionType().power, this.month, this.rate).energy);
        }
        if ($("[data-equivalent-money]").length > 0) {
            $("[data-equivalent-money]").text(super.resultCalc(super.getCollectionType().power, this.month, this.rate).money);
        }

        if ($("#default-energy-equivalent-graph").length > 0) {
            $("#default-energy-equivalent-graph").css({width : super.resultCalc(this.getCollectionType().power, this.month, this.rate).energy * 100 / 162+"%"});
        }
        if ($("#default-money-equivalent-graph").length > 0) {
            $("#default-money-equivalent-graph").css({width : super.resultCalc(this.getCollectionType().power, this.month, this.rate).money * 100 / 738.72+"%"});
        }
        if ($("[data-difference]").length > 0) {
            $("[data-difference]").text(((super.resultCalc(this.getCollection().power, this.month, this.rate).money - super.resultCalc(super.getCollectionType().power, this.month, this.rate).money) / super.resultCalc(this.getCollection().power, this.month, this.rate).money * 100).toFixed(0));
        }
    }

    changeValue() {
        DocReady(() => {
            this.initValue();

            $("[data-price-select]").on("change", () => {
                this.initValue();
            });

            $("[data-select-type-lamp]").on("change", () => {
                if ($("[data-difference]").length > 0) {
                    $("[data-difference]").text(((super.resultCalc(this.getCollection().power, this.month, this.rate).money - super.resultCalc(super.getCollectionType().power, this.month, this.rate).money) / super.resultCalc(this.getCollection().power, this.month, this.rate).money * 100).toFixed(0));
                }
            });
        });
    }
}

let incandescentValue = new IncandescentLamp();

incandescentValue.changeValue();