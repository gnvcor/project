'use strict';

import Convert from '../Convert/Convert';

export default class ResultCalculate extends Convert {
    constructor() {
        super();

        this.quantity = 1; //���������� ���� (��)
        this.time = 3; //����� ������ � ���� (�����)
        this.rate = 4.56; //����� (���/�)
        this.month = 1; //1 ����� (�� ���������)
    }

    resultCalc(power, month, rate) {
        var energy = ((this.quantity * power * this.time * 30) / 1000) * month;
        var money = energy * rate;

        return {
            energy : energy.toFixed(2),
            money : money.toFixed(2)
        }
    }
}