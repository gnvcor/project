'use strict';

import DocReady from 'es6-docready';
import ResultCalculate from './ResultCalculate';

export default class SelectTypeLamp extends ResultCalculate {
    constructor() {
        super();

        this.typeLamp = {
            'Светодиодная лампа' : {
                dependentValue : {
                    20 : {
                        power : 3,
                        price : 115
                    },
                    40 : {
                        power : 5,
                        price : 145
                    },
                    50 : {
                        power : 6,
                        price : 195
                    },
                    60 : {
                        power : 7,
                        price : 225
                    },
                    75 : {
                        power : 9,
                        price : 268
                    },
                    100 : {
                        power : 12,
                        price : 295
                    },
                    150 : {
                        power : 18,
                        price : 529
                    }
                },
                hoursWorking : 50000
            },
            'Газоразрядная лампа' : {
                dependentValue : {
                    20 : {
                        power : 10,
                        price : 1000
                    },
                    40 : {
                        power : 20,
                        price : 1100
                    },
                    50 : {
                        power : 30,
                        price : 1200
                    },
                    60 : {
                        power : 40,
                        price : 1300
                    },
                    75 : {
                        power : 50,
                        price : 1400
                    },
                    100 : {
                        power : 60,
                        price : 1500
                    },
                    150 : {
                        power : 70,
                        price : 1600
                    }
                },
                hoursWorking : 10000
            }
        };
    }

    getCollectionType() {
        for (var key in this.typeLamp) {
            if ($("[data-select-type-lamp]").val() == key) {
                for (var inKey in this.typeLamp[key]) {
                    if ('dependentValue' == String(inKey)) {
                        for (var inInKey in this.typeLamp[key][inKey]) {
                            if (inInKey == $("#equivalent-price").attr("data-equivalent")) {
                                return {
                                    name : key,
                                    power : this.typeLamp[key][inKey][inInKey].power,
                                    price : this.typeLamp[key][inKey][inInKey].price,
                                    hoursWorking : this.typeLamp[key].hoursWorking
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    changeValueType() {
        DocReady(() => {
            $("[data-select-type-lamp]").on("change", () => {
                $("[data-equivalent-power]").text(this.getCollectionType().power);
                $("[data-equivalent-price]").text(super.getConvertValue(this.getCollectionType().price));

                $("[data-hours-working]").text(super.getConvertValue(this.getCollectionType().hoursWorking));
                $("[data-select-title-lamp]").text(super.getConvertValue(this.getCollectionType().name));

                $("[data-equivalent-energy]").text(super.resultCalc(this.getCollectionType().power, this.month, this.rate).energy);
                $("[data-equivalent-money]").text(super.resultCalc(this.getCollectionType().power, this.month, this.rate).money);

                $("#default-energy-equivalent-graph").css({width : super.resultCalc(this.getCollectionType().power, this.month, this.rate).energy * 100 / 162+"%"});
                $("#default-money-equivalent-graph").css({width : super.resultCalc(this.getCollectionType().power, this.month, this.rate).money * 100 / 738.72+"%"});
            });
        });
    }
}

let typeValue = new SelectTypeLamp();

typeValue.changeValueType();