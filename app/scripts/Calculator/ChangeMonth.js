'use strict';

import IncandescentLamp from './IncandescentLamp';
import DocReady from 'es6-docready';

export default class ChangeMonth extends IncandescentLamp {
    constructor() {
        super();
    }

    changeMonth() {
        DocReady(() => {
            $("[data-month]").on("click", () => {
                $("[data-month]").addClass("active");
                $("[data-year]").removeClass("active");
                this.month = 1;

                super.changeValue();
                super.changeValueType();
            });

            $("[data-year]").on("click", () => {
                $("[data-year]").addClass("active");
                $("[data-month]").removeClass("active");
                this.month = 12;

                super.changeValue();
                super.changeValueType();
            });

            $("[data-rate]").on("change", () => {
                var value = $("[data-rate]").val();

                if (value <= 0.5) {
                    value = 0.5;
                    $("[data-rate]").val(0.5);
                }

                if (value >= 100) {
                    value = 100;
                    $("[data-rate]").val(100);
                }

                this.rate = value;

                super.changeValue();
                super.changeValueType();
            });
        });
    }
}

let month = new ChangeMonth();

month.changeMonth();