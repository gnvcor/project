'use strict';

import DocReady from 'es6-docready'

export class OpenText {
    constructor() {

    }

    resize() {
        let container = $(".header-text");

        container.each(function() {
            let hide = $(this).find(".header-text__hide");
            let isHide = hide.find(".header-text__hide-cont-height");

            if (hide.is(".open")) {
                hide.css({"height" : isHide.outerHeight()});
            }
        });
    }

    open() {
        DocReady(() => {
            let container = $(".header-text");

            container.each(function() {
                let hide = $(this).find(".header-text__hide");
                let current = $(this).find("[data-open]");
                let isHide = hide.find(".header-text__hide-cont-height");

                current.on("click", function() {
                    $(this).toggleClass("active");

                    if (hide.height() == 0) {
                        hide.addClass("open");
                        hide.css({"height" : isHide.outerHeight()});
                    } else {
                        hide.removeClass("open");
                        hide.css({"height" : 0});
                    }

                    let t1 = "Читать целиком";
                    let t2 = "Свернуть";
                    let self = $(this);

                    if ($(this).text() == t1){
                        $(self).text(t2);
                    } else {
                        $(self).text(t1);
                    }
                });
            });
        });
    }
}

let open = new OpenText();

open.open();

$(window).on("resize", () => {
    open.resize();
});