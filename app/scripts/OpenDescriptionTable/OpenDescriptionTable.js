'use strict';

import DocReady from 'es6-docready'

export class OpenDescriptionTable {
    constructor() {

    }

    resize() {
        if ($(".table-description__hide").is(".open")) {
            $(".table-description__hide").css({"height" : $(".table-description__items").outerHeight()});
        }
    }

    open() {
        DocReady(() => {
            let current = $("[data-open-table]");

            current.on("click", function() {
                current.toggleClass("active");

                if ($(".table-description__hide").height() == 0) {
                    $(".table-description__hide").addClass("open");
                    $(".table-description__hide").css({"height" : $(".table-description__hide .table-description__items").outerHeight()});
                } else {
                    $(".table-description__hide").removeClass("open");
                    $(".table-description__hide").css({"height" : 0});
                }

                let t1 = "Показать все характеристики";
                let t2 = "Скрыть все характеристики";
                let self = $(this);

                if ($(this).text() == t1){
                    $(self).text(t2);
                } else {
                    $(self).text(t1);
                }
            });
        });
    }
}

let open = new OpenDescriptionTable();

open.open();

$(window).on("resize", () => {
    open.resize();
});