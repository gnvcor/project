'use strict';

import DocReady from 'es6-docready';
import '../../../vendor/select2/dist/js/select2.js';

export class Select {
    constructor(selector) {
        this.selector = selector;
    }

    selectInit() {
        DocReady( () => {
            $(this.selector).select2({
                minimumResultsForSearch: -1
            });
        } );
    }
}

let select = new Select('[data-select]');

select.selectInit();

let selectSort = new Select('[data-select-sort]');

selectSort.selectInit();