'use strict';

export default class Convert {
    constructor() {

    }

    getConvertValue(value) {
        value += "";
        value = new Array(4 - value.length % 3).join("U") + value;
        return value.replace(/([0-9U]{3})/g, "$1 ").replace(/U/g, "");
    }
}