'use strict';

import DocReady from 'es6-docready';
import '../../../vendor/fancybox/source/jquery.fancybox.js';
import ZoomPhoto from '../Detail/ZoomPhoto'

export class Modal extends ZoomPhoto{
    constructor(selector, maxHeight) {
        super();
        this.selector = selector;
        this.maxHeight = maxHeight;
    }

    modalInit() {
        DocReady( () => {
            $(this.selector).fancybox({
                maxWidth	: 970,
                maxHeight	: this.maxHeight,
                fitToView	: false,
                width		: '100%',
                autoSize	: false,
                'padding' : [15, 0, 15, 0],
                beforeShow: () => {
                    //super.setValue();
                    super.zoomInit("#zoom_02", "gallery_02");
                    $(".modal").parent().css({"display" : "block"});
                    $(".modal").parent().parent().parent().parent().parent().addClass("modal__mobile");
                },
                afterClose: () => {
                    $(".zoomContainer").last().remove();
                }
            });
        } );
    }
}

let modal = new Modal('.various', 497);

modal.modalInit();

let modalFastClick = new Modal('.fast-click', 321);

modalFastClick.modalInit();