'use strict';

var params = {
  env: process.env.NODE_ENV || 'development'
}

// Require local webpack
var webpack = require('webpack');
var autoprefixer = require('autoprefixer-stylus');

// Externals plugins
var plugins = {
  bower: require("bower-webpack-plugin"),
  extText: require("extract-text-webpack-plugin"),
  stylus: {
    rupture: require("rupture")
  }
}
module.exports = {
  context: __dirname,
  entry: {
    app: __dirname + '/app'
  },

  // Watcher
  watch: params.env == 'development',
  watchOptions: {
    aggregateTimeout: 50
  },

  // Resolve params
  resolve: {
    root: __dirname,
    modulesDirectories: [
      __dirname,
      __dirname + '/vendor',
      __dirname + '/node_modules'
    ],
    packageFiles: ['package.json', 'bower.json', '.bower.json'],
    extensions: ['', '.js', '.ts', '.styl', '.css', '.jsx']
  },

  resolveLoader: {
    root: __dirname,
    modulesDirectories: [__dirname + "/node_modules"]
  },

  // Output params
  output: {
    path: __dirname,
    publicPath: '../',
    filename: '/dist/[name].js'
  },

  // Loaders params
  module: {
    loaders: [
      {
        test: /\.(styl|css)$/,
        loader: plugins.extText.extract('stylus', 'css-loader!stylus-loader')
      }, {
        test: /\.ts(x?)$/,
        exclude: /(node_modules|components)/,
        loader: 'babel-loader!ts-loader'
      }, {
        test: /\.js$/,
        exclude: /(node_modules|vendor)/,
        loader: 'babel',
        query: {
          cacheDirectory: true,
          plugins: ['transform-decorators-legacy'],
          presets: [
            require.resolve('babel-preset-es2015'),
            require.resolve('babel-preset-stage-0')
          ]
        }
      }, {
        test: /\.jpe?g$|\.gif$|\.png$|\.svg$|\.woff$|\.ttf$|\.wav$|\.mp3$/,
        loader: "file"
      },
      {
        test: /\.jsx$/,
        loader: "babel",
        query: {
          presets: ['es2015', 'react', 'stage-0'],
          "plugins": [
            ["transform-decorators-legacy"]
          ]
        }

      }
    ]
  },
  stylus: {
    use: [
      plugins.stylus.rupture(),
      autoprefixer()
    ]
  },
  node: {
    fs: 'empty'
  },
  // Plugins
  plugins: [
    new webpack.ProvidePlugin({
      _: "underscore",
      $: "jquery",
      jQuery: "jquery",
      "window.jQuery": "jquery",
      "React": "react"
    }),
    new plugins.extText("./dist/index.css", {
      allChunks: true
    }),
    new plugins.bower({
      modulesDirectories: [__dirname + '/vendor'],
      manifestFiles: ".bower.json",
      includes: /.*/,
      excludes: [],
      searchResolveModulesDirectories: true
    }),
    new webpack.optimize.UglifyJsPlugin()
  ]
};

