//css
import 'vendor/owl.carousel/dist/assets/owl.carousel.css'
import 'vendor/fancybox/source/jquery.fancybox.css'
import 'app/styles/css/select2.css'
import 'app/styles/css/ion.rangeSlider.css'
import 'app/styles/css/ion.rangeSlider.skinHTML5.css'

//styles
import 'app/styles/common.styl'

//scripts
import 'app/library/tab.js' //Import tab bootstrap
import 'app/scripts/app.js'